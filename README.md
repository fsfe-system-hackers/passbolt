<!--
SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/passbolt/00_README)


# Passbolt

[Passbolt](https://passbolt.com/) is a Free Software password storage and management service. The FSFE's installation is on [pass.fsfe.org](https://pass.fsfe.org) and available to people who are concerned with sensible passwords used by multiple people in various teams. It is a web-based service, but uses GPG in the background that encrypts all passwords securely.

## Usage

Documentation about this service can be found [in our documentation](https://docs.fsfe.org/en/repodocs/passbolt/passbolt) and on the official [passbolt website](https://help.passbolt.com/).

## Specifics

This instance mainly uses the [official deployment scripts](https://github.com/passbolt/passbolt_docker), but adds some custom changes:

* Basic auth password for extra protection
* Availability of database password in mariadb container to quickly execute mysql commands without password prompt
