---
title: Passbolt
description: The password storage and management service at FSFE
---

## Passbolt

[Passbolt](https://passbolt.com/) is a Free Software password storage and management service. The FSFE's installation is on [pass.fsfe.org](https://pass.fsfe.org/) and available to people who are concerned with sensible passwords used by multiple people in various teams. It is a web-based service, but uses GPG in the background that encrypts all passwords securely.

The basic idea is that every user creates a new GPG key upon registration, only used by Passbolt. The secret key will be stored within a browser add-on. Every password a user has access to will be encrypted with this key.

The deployment code is [here](https://git.fsfe.org/fsfe-system-hackers/passbolt).

## Initial Setup

If you belong to the group of people who should have access to a selection of these passwords, you will be invited via e-mail. Then follow these steps carefully, as the security of the passwords and your access depends on it:

1.  Click on the "get started" button in the e-mail you received from passbolt.
2.  You will probably be asked to install the Passbolt Extension in your browser. It is available for Firefox and Chromium. If you are done, reload the page.
3.  You will be asked to verify the server's data. Make sure that you see https://pass.fsfe.org as domain and 4E477C5EA50C5CA2DF941805C438739EE8F30B36 as server key. Tick the confirmation box if applicable and click "Next"
    
4.  You will now create your own dedicated GPG for Passbolt. You don't need to provide more data, just click "Next"
5.  Enter a secure passphrase for your new key. **Please store it safely**! Nobody will be able to recover it for you.
    
6.  In the next step, you have to download the generated key. This is only possible now, and nobody can recover it! **Download it and store it securely**!
    
7.  As an additional security layer, you can generate a token. Set a colour you like, and memorise it as well as its 3-character representation. It will be shown next to the login and other password fields. If it's different than what you set initially, you will know that the server does not seem to be legitimate.
8.  Now you can log in with your passphrase. Next to the password field, you will see the security token.

Please note that your key is saved within your browser inside the extension, so it is bound to this device and browser. If you ever change browsers or want to set up another device, it is possible to import the key you've saved earlier.

If you're done, inform your happy technical contact at the FSFE, and ask to be added to the respective groups you should have access to.

## Usage

To **view** a password:

1.  Visit the service website.
2.  Afterwards, you will see your email and be able to enter the password of your key (not you general FSFE password). Please check the colour/text token next to the password field.
3.  You will see all passwords you have access to. You can filter via the groups on the left, or search for a password.
4.  In your browser, you can also click on the Passbolt extension to search within it directly. This may a neat shortcut for you.

To **add** a new password:

1.  Click on the blue "Create" button
2.  Enter a meaningful name, URL, user & password, and a description if necessary
    
3.  Select this password and share it with the group it belongs to. Please set it as the owner ("is owner") to make the passwords not depend on individual users.

As of now, there are not classical folders but only a flat list of passwords, separated into groups. A folder feature is in passbolt's pipeline though.